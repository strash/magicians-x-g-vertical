extends TextureRect


# уровень
var level: int
# вид иконки
var type: int
# координаты в паттерне
var is_active: bool = false setget set_active  # selected
var is_neighbour: bool = false setget set_neighbour # selected like is_neighbour
# скорость твина
var speed: float = 0.15
var _t: bool


# BUILTINS -------------------------


func _ready() -> void:
	set_icon_texture()


# METHODS -------------------------


func set_icon_texture() -> void:
	self.texture = load("res://Resources/Sprites/MatchThree/ic_lvl_%s/%s.png" % [level, type])
	self.rect_size = Vector2.ZERO
	self.rect_pivot_offset = self.rect_size / 2.0


# переместить на другую позицию на доске
func go_to(new_pos: Vector2) -> void:
	_t = ($Tween as Tween).interpolate_property(self, "rect_position", null, new_pos, speed)
	if not ($Tween as Tween).is_active():
		_t = ($Tween as Tween).start()


# удаление иконки с доски
func death(new_type: int, delay: float) -> void:
	type = new_type
	yield(get_tree().create_timer(delay), "timeout")
	_t = ($Tween as Tween).interpolate_property(self, "rect_scale", null, Vector2.ZERO, speed)
	if not ($Tween as Tween).is_active():
		_t = ($Tween as Tween).start()
	yield(get_tree().create_timer(speed * 3.0), "timeout")
	set_icon_texture()
	_t = ($Tween as Tween).interpolate_property(self, "rect_scale", null, Vector2.ONE, speed)
	if not ($Tween as Tween).is_active():
		_t = ($Tween as Tween).start()


# SETGET -------------------------


# setter for is_active
func set_active(state: bool) -> void:
	if state:
		($AnimationPlayer as AnimationPlayer).play("selected")
	else:
		($AnimationPlayer as AnimationPlayer).stop()
		self.rect_scale = Vector2.ONE
	is_active = state


# setter for is_neighbour
func set_neighbour(state: bool) -> void:
	if state:
		($AnimationPlayer as AnimationPlayer).play("neighboured")
	else:
		($AnimationPlayer as AnimationPlayer).stop()
		self.self_modulate.a = 1.0
	is_neighbour = state


# SIGNALS -------------------------


