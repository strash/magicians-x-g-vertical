extends Control


signal icons_deleted


var payload: Dictionary setget set_payload

# описание видимости блоков GUI
const GUI_SETUP: Dictionary = {
	"is_header_block_visible": true,
	"is_btn_back_block_visible": true,
	"is_score_block_visible": false,
	"is_level_block_visible": true,
	"is_footer_block_visible": false,
}


var Icon: PackedScene = load("res://Scenes/MatchThree/Components/MatchThreeIcon.tscn")


# Состояние клетки на доске
# EMPTY — ячейка не на доске
# USED — в ячейке есть иконка
enum GRID_CELL {
	EMPTY = 0,
	USED = 1,
}


# Объект с параметрами уровня
const PROPS: Array = [
	{
		ic_w = 60,
		ic_h = 60,
		variations = 3,
		gap_w = 21,
		gap_h = 18,
		offset_x = 0,
		offset_y = 0,
	},
	{
		ic_w = 73,
		ic_h = 75,
		variations = 4,
		gap_w = 10,
		gap_h = 10,
		offset_x = 0,
		offset_y = 0,
	},
	{
		ic_w = 61,
		ic_h = 61,
		variations = 4,
		gap_w = 20,
		gap_h = 20,
		offset_x = 0,
		offset_y = 0,
	},
]

# Ссылки на картинки бэкграундов
const BGS: PoolStringArray = PoolStringArray([
	"res://Resources/Sprites/MatchThree/bg_lvl_1.png",
	"res://Resources/Sprites/MatchThree/bg_lvl_2.png",
	"res://Resources/Sprites/MatchThree/bg_lvl_3.png",
])
# Ссылки на картинки досок
const BOARDS: PoolStringArray = PoolStringArray([
	"res://Resources/Sprites/MatchThree/board_lvl_1.png",
	"res://Resources/Sprites/MatchThree/board_lvl_2.png",
	"res://Resources/Sprites/MatchThree/board_lvl_3.png",
])

# Паттерн заполнения доски.
# Note. Соответствует enum GRID_CELL
# 0 - пустое ячейчка, 1 - ячейка заполняется иконкой
const PATTERNS = [
	[
		[1,1,1,1],
		[1,1,1,1],
		[1,1,1,1],
		[1,1,1,1],
	],
	[
		[1,1,1,1],
		[1,1,1,1],
		[1,1,1,1],
		[1,1,1,1],
	],
	[
		[1,1,1,1],
		[1,1,1,1],
		[1,1,1,1],
		[1,1,1,1],
	],
]

# Текущий уровень
var level = 1
# Сетка с иконками
var GRID: Array = []
var PURGATORY: Array = []


# BUILTINS -------------------------


func _input(event: InputEvent) -> void:
	if self.is_visible_in_tree() and event.is_pressed():
		if event is InputEventMouse and (event as InputEventMouse).button_mask == BUTTON_MASK_LEFT:
			var cursor_pos: Vector2 = (event as InputEventMouse).position - ($Board as TextureRect).rect_position
			for i in get_tree().get_nodes_in_group("match_three_icon_group"):
				if i.get_rect().has_point(cursor_pos):
					# активируем иконку
					set_active_icon(i)
					break


# METHODS -------------------------


# установка текстур уровня и расстановка иконок на доске по паттерну
func load_level(new_level: int) -> void:
	level = new_level
	# сначала чистим уровень
	clear_level()
	# расставляем текстуры
	($Board as TextureRect).texture = load(BOARDS[level - 1])
	($Board as TextureRect).rect_size = Vector2.ZERO # HACK: чтобы сбрасывался размер доски
	($Board as TextureRect).rect_position = (get_viewport_rect().size - ($Board as TextureRect).rect_size) / 2
	($BG as TextureRect).texture = load(BGS[level - 1])
	# расставляем новые иконки
	fill_grid()


# заполнение доски
func fill_grid() -> void:
	var board: TextureRect = $Board as TextureRect
	# считаем всякие размеры и координаты
	var icon_size: Vector2 = Vector2(PROPS[level - 1].ic_w, PROPS[level - 1].ic_h)
	var gap_size: Vector2 = Vector2(PROPS[level - 1].gap_w, PROPS[level - 1].gap_h)
	var grid_w: int = int(icon_size.x) * PATTERNS[level - 1][0].size() + int(gap_size.x) * (PATTERNS[level - 1][0].size() - 1)
	var grid_h: int = int(icon_size.y) * PATTERNS[level - 1].size() + int(gap_size.y) * (PATTERNS[level - 1].size() - 1)
	var grid_size: Vector2 = Vector2(grid_w, grid_h)

	# расставляем новые иконки
	var y = 0 # индекс по вертикали
	for rows in PATTERNS[level - 1]:
		GRID.append([])
		var x = 0 # индекс по горизонтали
		for item in rows:
			if item == GRID_CELL.USED:
				var icon: TextureRect = create_icon(icon_size, gap_size, grid_size, Vector2(x, y), board.rect_size)
				GRID[y].append({
					icon = icon,
					idx = Vector2(x, y),
					cell = GRID_CELL.USED,
					checked = false,
				})
				board.add_child(icon)
			elif item == GRID_CELL.EMPTY:
				GRID[y].append({
					icon = null,
					idx = Vector2(x, y),
					cell = GRID_CELL.EMPTY,
					checked = false,
				})
			x += 1
		y += 1


# создание иконки
func create_icon(icon_size: Vector2, gap_size: Vector2, grid_size: Vector2, idx: Vector2, board_size: Vector2) -> TextureRect:
	var icon: TextureRect = Icon.instance()
	var type: int = (randi() % PROPS[level - 1].variations) + 1
	var pos_x: float = icon_size.x * idx.x + gap_size.x * idx.x + (board_size.x - grid_size.x) / 2 + PROPS[level - 1].offset_x
	var pos_y: float = icon_size.y * idx.y + gap_size.y * idx.y + (board_size.y - grid_size.y) / 2 + PROPS[level - 1].offset_y
	icon.set("level", level)
	icon.set("type", type)
	icon.set_position(Vector2(pos_x, pos_y))
	return icon


# устанавливаем активную иконку и ее соседей
func set_active_icon(icon: TextureRect) -> void:
	# находим ячейку в сетку с выбранной иконкой
	var cell: Dictionary
	for row in GRID:
		for i in row:
			if i.icon == icon:
				cell = i
				break
		if cell.has("icon"):
			break
	# если иконка активна, то просто чиститм все состояния
	if cell.icon.get("is_active"):
		clear_icons_states()
	# если не активна
	else:
		# если попали в соседа, то двигаем иконки
		if cell.icon.get("is_neighbour"):
			var origins: Array = move_icons(cell)
			clear_icons_states()
			yield(get_tree().create_timer(0.3), "timeout")
			collect_twins(origins)
		# иначе делаем иконку активной
		else:
			clear_icons_states()
			cell.icon.call("set_active", true)
			find_sublings(cell.idx)


# подсвечиваем иконки в окрестности фон Неймана первого порядка
# 0 1 0
# 1 @ 1
# 0 1 0
func find_sublings(idx: Vector2) -> void:
	# TOP
	if idx.y - 1 >= 0 and GRID[idx.y - 1][idx.x].cell != GRID_CELL.EMPTY:
		GRID[idx.y - 1][idx.x].icon.call("set_neighbour", true)
	# RIGHT
	if idx.x + 1 <= GRID[0].size() - 1 and GRID[idx.y][idx.x + 1].cell != GRID_CELL.EMPTY:
		GRID[idx.y][idx.x + 1].icon.call("set_neighbour", true)
	# BOTTOM
	if idx.y + 1 <= GRID.size() - 1 and GRID[idx.y + 1][idx.x].cell != GRID_CELL.EMPTY:
		GRID[idx.y + 1][idx.x].icon.call("set_neighbour", true)
	# LEFT
	if idx.x - 1 >= 0 and GRID[idx.y][idx.x - 1].cell != GRID_CELL.EMPTY:
		GRID[idx.y][idx.x - 1].icon.call("set_neighbour", true)


# перемещение двух иконок между собой
func move_icons(neighbour_cell: Dictionary) -> Array:
	var active_cell: Dictionary
	var idx: Vector2 = neighbour_cell.idx
	# TOP
	if idx.y - 1 >= 0 and GRID[idx.y - 1][idx.x].cell != GRID_CELL.EMPTY and GRID[idx.y - 1][idx.x].icon.get("is_active"):
		active_cell = GRID[idx.y - 1][idx.x]
	# RIGHT
	elif idx.x + 1 <= GRID[0].size() - 1 and GRID[idx.y][idx.x + 1].cell != GRID_CELL.EMPTY and GRID[idx.y][idx.x + 1].icon.get("is_active"):
		active_cell = GRID[idx.y][idx.x + 1]
	# BOTTOM
	elif idx.y + 1 <= GRID.size() - 1 and GRID[idx.y + 1][idx.x].cell != GRID_CELL.EMPTY and GRID[idx.y + 1][idx.x].icon.get("is_active"):
		active_cell = GRID[idx.y + 1][idx.x]
	# RIGHT
	elif idx.x - 1 >= 0 and GRID[idx.y][idx.x - 1].cell != GRID_CELL.EMPTY and GRID[idx.y][idx.x - 1].icon.get("is_active"):
		active_cell = GRID[idx.y][idx.x - 1]

	var neighbour_icon: TextureRect = neighbour_cell.icon
	var active_icon: TextureRect = active_cell.icon
	var neighbour_pos: Vector2 = neighbour_icon.get("rect_position")
	var active_pos: Vector2 = active_icon.get("rect_position")
	# двигаем иконки
	neighbour_icon.call("go_to", active_pos)
	active_icon.call("go_to", neighbour_pos)
	# меняем иконки в ячейках
	neighbour_cell.icon = active_icon
	active_cell.icon = neighbour_icon
	# меняем состояния
	neighbour_icon.call("set_neighbour", false)
	neighbour_icon.call("set_active", true)
	active_icon.call("set_active", false)
	active_icon.call("set_neighbour", true)
	return [neighbour_cell, active_cell]


# ищем похожие и удаляем
func collect_twins(origins: Array) -> void:
	var type_1: int = origins[0].icon.get("type")
	var type_2: int = origins[1].icon.get("type")
	for i in origins:
		_find_twins(i, i.icon.get("type"))
	if PURGATORY.size() >= 3:
		if type_1 == type_2:
			emit_signal("icons_deleted", PURGATORY.size())
			get_node("/root/Main/GUI").call_deferred("set_score")
			for i in PURGATORY.size():
				PURGATORY[i].icon.call("death", (randi() % PROPS[level - 1].variations) + 1, i / 40.0)
		else:
			var count_1: int = 0
			var count_2: int = 0
			for i in PURGATORY:
				if i.icon.get("type") == type_1:
					count_1 += 1
				else:
					count_2 += 1
			var count_sum: int = count_1 if count_1 >= 3 else 0 + count_2 if count_2 >= 3 else 0
			if count_sum >= 3:
				emit_signal("icons_deleted", count_sum)
				get_node("/root/Main/GUI").call_deferred("set_score")
			for i in PURGATORY.size():
				if count_1 >= 3 and PURGATORY[i].icon.get("type") == type_1:
					PURGATORY[i].icon.call("death", (randi() % PROPS[level - 1].variations) + 1, i / 40.0)
				elif count_2 >= 3 and PURGATORY[i].icon.get("type") == type_2:
					PURGATORY[i].icon.call("death", (randi() % PROPS[level - 1].variations) + 1, i / 40.0)
	# чистим все после поиска
	for i in PURGATORY:
		i.checked = false
	PURGATORY.clear()


# нахождение похожих иконок
func _find_twins(cell: Dictionary, type: int) -> void:
	# TOP
	if cell.idx.y - 1 >= 0 and GRID[cell.idx.y - 1][cell.idx.x].cell == GRID_CELL.USED and GRID[cell.idx.y - 1][cell.idx.x].icon.get("type") == type and not GRID[cell.idx.y - 1][cell.idx.x].checked:
		GRID[cell.idx.y - 1][cell.idx.x].checked = true
		PURGATORY.append(GRID[cell.idx.y - 1][cell.idx.x])
		_find_twins(GRID[cell.idx.y - 1][cell.idx.x], type)
	# RIGHT
	if cell.idx.x + 1 <= GRID[cell.idx.y].size() - 1 and GRID[cell.idx.y][cell.idx.x + 1].cell == GRID_CELL.USED and GRID[cell.idx.y][cell.idx.x + 1].icon.get("type") == type and not GRID[cell.idx.y][cell.idx.x + 1].checked:
		GRID[cell.idx.y][cell.idx.x + 1].checked = true
		PURGATORY.append(GRID[cell.idx.y][cell.idx.x + 1])
		_find_twins(GRID[cell.idx.y][cell.idx.x + 1], type)
	# BOTTOM
	if cell.idx.y + 1 <= GRID.size() - 1 and GRID[cell.idx.y + 1][cell.idx.x].cell == GRID_CELL.USED and GRID[cell.idx.y + 1][cell.idx.x].icon.get("type") == type and not GRID[cell.idx.y + 1][cell.idx.x].checked:
		GRID[cell.idx.y + 1][cell.idx.x].checked = true
		PURGATORY.append(GRID[cell.idx.y + 1][cell.idx.x])
		_find_twins(GRID[cell.idx.y + 1][cell.idx.x], type)
	# LEFT
	if cell.idx.x - 1 >= 0 and GRID[cell.idx.y][cell.idx.x - 1].cell == GRID_CELL.USED and GRID[cell.idx.y][cell.idx.x - 1].icon.get("type") == type and not GRID[cell.idx.y][cell.idx.x - 1].checked:
		GRID[cell.idx.y][cell.idx.x - 1].checked = true
		PURGATORY.append(GRID[cell.idx.y][cell.idx.x - 1])
		_find_twins(GRID[cell.idx.y][cell.idx.x - 1], type)


# очистка состояний иконок
func clear_icons_states() -> void:
	for i in get_tree().get_nodes_in_group("match_three_icon_group"):
		i.call("set_active", false)
		i.call("set_neighbour", false)


# очистка уровня
func clear_level() -> void:
	for i in get_tree().get_nodes_in_group("match_three_icon_group"):
		i.queue_free()
	GRID.clear()


# SETGET -------------------------


func set_payload(dict: Dictionary) -> void:
	payload = dict
	load_level(payload.level)


# SIGNALS -------------------------


