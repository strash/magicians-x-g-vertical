extends Control


const Counter: PackedScene = preload("res://Scenes/Slot/Components/SlotCounter.tscn")


signal btn_back_pressed
signal btn_spin_pressed
signal btn_autospin_pressed


var is_header_block_visible: bool = true setget set_header_block
var is_footer_block_visible: bool = true setget set_footer_block
var is_score_block_visible: bool = true setget set_score_block
var is_level_block_visible: bool = true setget set_level_block
var is_btn_back_block_visible: bool = true setget set_btn_back_block

# отображать ли всплывающую сумму выигрыша/проигрыша
export var is_popping_counter_visible: bool = true


# CUSTOMISING
const FOOTER_EXTRA_OFFSET: float = 200.0 # extra offset for hiding footer in "set_hide_controls()"
var HEADER_POS: float # header position.y
var BTN_BACK_POS: float # button back position.y
var BET_POS: float # bet position.y
var WIN_POS: float # win score position.y
var MAXBET_POS: float # maxbet position.y
var AUTOSPIN_POS: float # button autospin position.y
var SPIN_POS: float # button spin position.y
var PROGRESS_POS_HIDE: float
const SCORE_TWEEN_SPEED: float = 1.0


# меняется сигналами из Game через Main
var spinning: bool = false
var auto_spinning: bool = false


var SCORE: int = 300000 setget set_SCORE # общий счетчик
var SCORE_WIN: int = 0 setget set_SCORE_WIN # счетчик выигранных очков
var LEVEL: int = 1 # уровень на шкале уровня
var LEVEL_SCORE: int = 0 setget set_LEVEL_SCORE # выигранные очки
var LEVEL_STEP: int = 300 # шаг уровня
const CHANSE_TO_WIN: int = 15 # шанс на победу. условно в процентах
const RATE: float = 2.0 # повышающий коэффициент при выигрыше

const MIN_BET: float = 10.0 # минимальный порог ставки
const MAX_BET: float = 200.0 # максимальный порог ставки
const BET_STEP: float= 10.0 # шаг инкремента/дикремента
var BET: float= 100.0 # текущий коэффициент ставки

var REGEX: RegEx = RegEx.new()

const TWEEN_SPEED: float = 0.3
var _t: bool


# BUILTINS -------------------------


func _ready() -> void:
	var _r: int = REGEX.compile("(\\d)(?=(\\d\\d\\d)+([^\\d]|$))")
	set_level_progress(0)
	set_SCORE(SCORE)
	if not ($Footer/Bet as Control).is_visible_in_tree() and not ($Footer/BtnMaxBet as Button).is_visible_in_tree():
		BET = MAX_BET
	HEADER_POS = ($Header as Control).get_rect().position.y
	BTN_BACK_POS = ($Header/BtnBack as Button).get_rect().position.y
	BET_POS = ($Footer/Bet as Control).get_rect().position.y
	WIN_POS = ($Footer/BgWin as TextureRect).get_rect().position.y
	MAXBET_POS = ($Footer/BtnMaxBet as Button).get_rect().position.y
	AUTOSPIN_POS = ($Footer/BtnAutoSpin as Button).get_rect().position.y
	SPIN_POS = ($Footer/BtnSpin as Button).get_rect().position.y
	PROGRESS_POS_HIDE = ($Footer/AutoSpinProgress as TextureProgress).get_rect().position.y
	($Footer/Bet/Label as Label).text = str(BET)
	($Footer/AutoSpinProgress as TextureProgress).min_value = 0.0
	($Footer/AutoSpinProgress as TextureProgress).max_value = Global.get("AUTO_SPIN_MAX")
	($Footer/AutoSpinProgress as TextureProgress).step = Global.get("AUTO_SPIN_MAX") / 500.0


# METHODS -------------------------


# создание всплывающей суммы выигрыша/проигрыша
func _set_popping_counter(increase: bool, count: int) -> void:
	if is_popping_counter_visible:
		var counter: Label = Counter.instance() as Label
		counter.set("increase", increase)
		counter.set("count", count)
		get_parent().add_child(counter)


# установка очков
func set_score() -> void:
	if randi() % 100 <= CHANSE_TO_WIN * BET / 45 + CHANSE_TO_WIN:
		var count: int = floor(RATE * BET + RATE * LEVEL) as int
		_t = ($Tween as Tween).interpolate_method(self, "set_SCORE", SCORE, SCORE + count, SCORE_TWEEN_SPEED)
		_t = ($Tween as Tween).interpolate_method(self, "set_SCORE_WIN", SCORE_WIN, SCORE_WIN + count, SCORE_TWEEN_SPEED)
		set_level_progress(count)
		_set_popping_counter(true, count)
	else:
		_t = ($Tween as Tween).interpolate_method(self, "set_SCORE", SCORE, SCORE - BET, SCORE_TWEEN_SPEED / 3.0)
		_set_popping_counter(false, int(BET))
	if not ($Tween as Tween).is_active():
		_t = ($Tween as Tween).start()


# установка подарочных очков
func set_gift_score() -> void:
	_t = ($Tween as Tween).interpolate_method(self, "set_SCORE", SCORE, SCORE + Global.get("GIFT_PRICE"), SCORE_TWEEN_SPEED)
	_set_popping_counter(true, Global.get("GIFT_PRICE"))
	_t = ($Tween as Tween).interpolate_property($Header/BtnGift, "rect_position:y", ($Header/BtnGift as Button).rect_position.y, -200.0, TWEEN_SPEED, Tween.TRANS_CUBIC)
	if not ($Tween as Tween).is_active():
		_t = ($Tween as Tween).start()


# установка уровня
func set_level_progress(count: int) -> void:
	var level_progress: TextureProgress = $Header/Level as TextureProgress
	var old_value: float = level_progress.value
	var new_value: float
	if LEVEL_SCORE + count > LEVEL * LEVEL_STEP:
		var first_half: int = LEVEL * LEVEL_STEP - LEVEL_SCORE # количество очков до полного уровня
		var second_half: int = count - first_half # количество оставшихся очков для следующего уровня
		var first_time: float = float(first_half) * SCORE_TWEEN_SPEED / count
		var second_time: float = second_half * SCORE_TWEEN_SPEED / count
		_t = ($Tween as Tween).interpolate_property(level_progress, "value", old_value, level_progress.max_value, first_time)
		_t = ($Tween as Tween).interpolate_method(self, "set_LEVEL_SCORE", LEVEL_SCORE, LEVEL_SCORE + first_half, first_time)
		if not ($Tween as Tween).is_active():
			_t = ($Tween as Tween).start()
		yield(get_tree().create_timer(first_time), "timeout")
		LEVEL += 1
		new_value = float(second_half) / (LEVEL * LEVEL_STEP) * level_progress.max_value
		_t = ($Tween as Tween).interpolate_property(level_progress, "value", 0.0, new_value, second_time)
		_t = ($Tween as Tween).interpolate_method(self, "set_LEVEL_SCORE", 0, second_half, second_time)
		if not ($Tween as Tween).is_active():
			_t = ($Tween as Tween).start()
	else:
		new_value = float(LEVEL_SCORE + count) / (LEVEL * LEVEL_STEP) * level_progress.max_value
		_t = ($Tween as Tween).interpolate_property(level_progress, "value", old_value, new_value, SCORE_TWEEN_SPEED)
		_t = ($Tween as Tween).interpolate_method(self, "set_LEVEL_SCORE", LEVEL_SCORE, LEVEL_SCORE + count, SCORE_TWEEN_SPEED)
		if not ($Tween as Tween).is_active():
			_t = ($Tween as Tween).start()
	($Header/Level/IcLevel/LevelLabel as Label).text = "%s" % LEVEL


# отображение автоспина
func show_hide_autospin_progress(show: bool) -> void:
	var footer_size: float = ($Footer as Control).get_rect().size.y
	var progress_pos_show: float = (footer_size - ($Footer/AutoSpinProgress as TextureProgress).get_rect().size.y) / 2
	if show:
		_t = ($Tween as Tween).interpolate_property($Footer/AutoSpinProgress, "rect_position:y", PROGRESS_POS_HIDE, progress_pos_show, TWEEN_SPEED)
		_t = ($Tween as Tween).interpolate_property($Footer/Bet, "rect_position:y", BET_POS, BET_POS + footer_size, TWEEN_SPEED)
		_t = ($Tween as Tween).interpolate_property($Footer/BgWin, "rect_position:y", WIN_POS, WIN_POS + footer_size, TWEEN_SPEED)
		_t = ($Tween as Tween).interpolate_property($Footer/BtnMaxBet, "rect_position:y", MAXBET_POS, MAXBET_POS + footer_size, TWEEN_SPEED)
		_t = ($Tween as Tween).interpolate_property($Footer/BtnAutoSpin, "rect_position:y", AUTOSPIN_POS, AUTOSPIN_POS + footer_size, TWEEN_SPEED)
		_t = ($Tween as Tween).interpolate_property($Footer/BtnSpin, "rect_position:y", SPIN_POS, SPIN_POS + footer_size, TWEEN_SPEED)
		_t = ($Tween as Tween).interpolate_property($Footer/AutoSpinProgress, "modulate:a", 0.0, 1.0, TWEEN_SPEED)
		_t = ($Tween as Tween).interpolate_property($Footer/Bet, "modulate:a", 1.0, 0.0, TWEEN_SPEED)
		_t = ($Tween as Tween).interpolate_property($Footer/BtnMaxBet, "modulate:a", 1.0, 0.0, TWEEN_SPEED)
		_t = ($Tween as Tween).interpolate_property($Footer/BtnAutoSpin, "modulate:a", 1.0, 0.0, TWEEN_SPEED)
		_t = ($Tween as Tween).interpolate_property($Footer/BtnSpin, "modulate:a", 1.0, 0.0, TWEEN_SPEED)
	else:
		_t = ($Tween as Tween).interpolate_property($Footer/AutoSpinProgress, "rect_position:y", progress_pos_show, PROGRESS_POS_HIDE, TWEEN_SPEED)
		_t = ($Tween as Tween).interpolate_property($Footer/Bet, "rect_position:y", BET_POS + footer_size, BET_POS, TWEEN_SPEED)
		_t = ($Tween as Tween).interpolate_property($Footer/BgWin, "rect_position:y", WIN_POS + footer_size, WIN_POS, TWEEN_SPEED)
		_t = ($Tween as Tween).interpolate_property($Footer/BtnMaxBet, "rect_position:y", MAXBET_POS + footer_size, MAXBET_POS, TWEEN_SPEED)
		_t = ($Tween as Tween).interpolate_property($Footer/BtnAutoSpin, "rect_position:y", AUTOSPIN_POS + footer_size, AUTOSPIN_POS, TWEEN_SPEED)
		_t = ($Tween as Tween).interpolate_property($Footer/BtnSpin, "rect_position:y", SPIN_POS + footer_size, SPIN_POS, TWEEN_SPEED)
		_t = ($Tween as Tween).interpolate_property($Footer/AutoSpinProgress, "modulate:a", 1.0, 0.0, TWEEN_SPEED)
		_t = ($Tween as Tween).interpolate_property($Footer/Bet, "modulate:a", 0.0, 1.0, TWEEN_SPEED)
		_t = ($Tween as Tween).interpolate_property($Footer/BtnMaxBet, "modulate:a", 0.0, 1.0, TWEEN_SPEED)
		_t = ($Tween as Tween).interpolate_property($Footer/BtnAutoSpin, "modulate:a", 0.0, 1.0, TWEEN_SPEED)
		_t = ($Tween as Tween).interpolate_property($Footer/BtnSpin, "modulate:a", 0.0, 1.0, TWEEN_SPEED)
	if not ($Tween as Tween).is_active():
		_t = ($Tween as Tween).start()
	($Footer/AutoSpinProgress as TextureProgress).value = 0


# изменеие счетчика количества и прогресса автоспинов
func change_autospin_count() -> void:
	var progress: TextureProgress = ($Footer/AutoSpinProgress as TextureProgress)
	var progress_value: float = progress.value
	_t = ($Tween as Tween).interpolate_property(progress, "value", progress_value, Global.get("AUTO_SPIN_COUNT"), Global.get("SPIN_TIME") + 1.5)
	if not ($Tween as Tween).is_active():
		_t = ($Tween as Tween).start()
	($Footer/AutoSpinProgress/Label as Label).text = "%s / %s" % [Global.get("AUTO_SPIN_COUNT"), Global.get("AUTO_SPIN_MAX")]


# форматирование числа, проставление знаков между порядками
func format_number(number: int) -> String:
	var num_string: String = str(number)
	var formated_number: String = REGEX.sub(num_string, "$1,", true)
	return formated_number


# SETGET -------------------------


# отображение хедера
func set_header_block(state: bool) -> void:
	is_header_block_visible = state
	var pos_hide: float = -150.0
	if state:
		_t = ($Tween as Tween).interpolate_property($Header, "rect_position:y", null, HEADER_POS, TWEEN_SPEED)
	else:
		_t = ($Tween as Tween).interpolate_property($Header, "rect_position:y", null, pos_hide, TWEEN_SPEED)
	if not ($Tween as Tween).is_active():
		_t = ($Tween as Tween).start()


func set_footer_block(state: bool) -> void:
	is_footer_block_visible = state
	var footer_size: float = ($Footer as Control).get_rect().size.y
	var footer_pos: float = get_rect().size.y - footer_size
	if state:
		_t = ($Tween as Tween).interpolate_property($Footer, "rect_position:y", null, footer_pos, TWEEN_SPEED)
	else:
		_t = ($Tween as Tween).interpolate_property($Footer, "rect_position:y", null, footer_pos + footer_size + FOOTER_EXTRA_OFFSET, TWEEN_SPEED)
	if not ($Tween as Tween).is_active():
		_t = ($Tween as Tween).start()


# отображение денег
func set_score_block(state: bool) -> void:
	is_score_block_visible = state
	if state:
		_t = ($Tween as Tween).interpolate_property($Header/Score, "modulate:a", null, 1.0, TWEEN_SPEED)
	else:
		_t = ($Tween as Tween).interpolate_property($Header/Score, "modulate:a", null, 0.0, TWEEN_SPEED)
	if not ($Tween as Tween).is_active():
		_t = ($Tween as Tween).start()


# отображение уровня
func set_level_block(state: bool) -> void:
	is_level_block_visible = state
	if state:
		_t = ($Tween as Tween).interpolate_property($Header/Level, "modulate:a", null, 1.0, TWEEN_SPEED)
	else:
		_t = ($Tween as Tween).interpolate_property($Header/Level, "modulate:a", null, 0.0, TWEEN_SPEED)
	if not ($Tween as Tween).is_active():
		_t = ($Tween as Tween).start()


# отображение кнопки назад
func set_btn_back_block(state: bool) -> void:
	is_btn_back_block_visible = state
	var pos_hide: float = -500.0
	if state:
		_t = ($Tween as Tween).interpolate_property($Header/BtnBack, "rect_position:y", null, BTN_BACK_POS, TWEEN_SPEED)
	else:
		_t = ($Tween as Tween).interpolate_property($Header/BtnBack, "rect_position:y", null, BTN_BACK_POS + pos_hide, TWEEN_SPEED)
	if not ($Tween as Tween).is_active():
		_t = ($Tween as Tween).start()


func set_SCORE(num: int) -> void:
	SCORE = num
	if ($Header/Score/Label as Label).is_visible_in_tree():
		($Header/Score/Label as Label).text = format_number(num)


func set_SCORE_WIN(num: int) -> void:
	SCORE_WIN = num
	if ($Footer/BgWin/ScoreWin as Label).is_visible_in_tree():
		($Footer/BgWin/ScoreWin as Label).text = "%s" % format_number(num)


func set_LEVEL_SCORE(num: int) -> void:
	LEVEL_SCORE = num
	if ($Header/Level/LevelScoreLabel as Label).is_visible_in_tree():
		($Header/Level/LevelScoreLabel as Label).text = "%s/%s" % [format_number(num), format_number(LEVEL * LEVEL_STEP)]


# SIGNALS -------------------------


func _on_BtnBack_pressed() -> void:
	if not spinning and not auto_spinning:
		emit_signal("btn_back_pressed")


func _on_BtnSpin_button_down() -> void:
	($TimerSpinLongPress as Timer).start()


func _on_BtnSpin_button_up() -> void:
	if not spinning and not auto_spinning:
		emit_signal("btn_spin_pressed")
		($TimerSpinLongPress as Timer).stop()


func _on_Timer_timeout() -> void:
	if not spinning and not auto_spinning:
		emit_signal("btn_autospin_pressed")
		show_hide_autospin_progress(true)
		change_autospin_count()


func _on_BtnMinus_pressed() -> void:
	if not spinning and not auto_spinning:
		BET = BET - BET_STEP if BET > MIN_BET else MIN_BET
		($Footer/Bet/Label as Label).text = str(BET)

		var btn_min_opacity: float = ($Footer/Bet/BtnMinus as Button).modulate.a
		var btn_max_opacity: float = ($Footer/Bet/BtnPlus as Button).modulate.a
		if BET == MIN_BET:
			_t = ($Tween as Tween).interpolate_property($Footer/Bet/BtnMinus, "modulate:a", btn_min_opacity, 0.0, TWEEN_SPEED)
		else:
			_t = ($Tween as Tween).interpolate_property($Footer/Bet/BtnMinus, "modulate:a", btn_min_opacity, 1.0, TWEEN_SPEED)
		_t = ($Tween as Tween).interpolate_property($Footer/Bet/BtnPlus, "modulate:a", btn_max_opacity, 1.0, TWEEN_SPEED)

		if not ($Tween as Tween).is_active():
			_t = ($Tween as Tween).start()


func _on_BtnPlus_pressed() -> void:
	if not spinning and not auto_spinning:
		BET = BET + BET_STEP if BET < MAX_BET else MAX_BET
		($Footer/Bet/Label as Label).text = str(BET)

		var btn_max_opacity: float = ($Footer/Bet/BtnPlus as Button).modulate.a
		var btn_min_opacity: float = ($Footer/Bet/BtnMinus as Button).modulate.a
		if BET == MAX_BET:
			_t = ($Tween as Tween).interpolate_property($Footer/Bet/BtnPlus, "modulate:a", btn_max_opacity, 0.0, TWEEN_SPEED)
		else:
			_t = ($Tween as Tween).interpolate_property($Footer/Bet/BtnPlus, "modulate:a", btn_max_opacity, 1.0, TWEEN_SPEED)
		_t = ($Tween as Tween).interpolate_property($Footer/Bet/BtnMinus, "modulate:a", btn_min_opacity, 1.0, TWEEN_SPEED)

		if not ($Tween as Tween).is_active():
			_t = ($Tween as Tween).start()


func _on_BtnAutoSpin_pressed() -> void:
	if not spinning and not auto_spinning:
		emit_signal("btn_autospin_pressed")
		show_hide_autospin_progress(true)
		change_autospin_count()


func _on_BtnGift_pressed() -> void:
	set_gift_score()


func _on_BtnMaxBet_pressed() -> void:
	if not spinning and not auto_spinning:
		BET = MAX_BET
		($Footer/Bet/Label as Label).text = str(BET)
		var btn_max_opacity: float = ($Footer/Bet/BtnPlus as Button).modulate.a
		var btn_min_opacity: float = ($Footer/Bet/BtnMinus as Button).modulate.a
		_t = ($Tween as Tween).interpolate_property($Footer/Bet/BtnPlus, "modulate:a", btn_max_opacity, 0.0, TWEEN_SPEED)
		_t = ($Tween as Tween).interpolate_property($Footer/Bet/BtnMinus, "modulate:a", btn_min_opacity, 1.0, TWEEN_SPEED)
		if not ($Tween as Tween).is_active():
			_t = ($Tween as Tween).start()


