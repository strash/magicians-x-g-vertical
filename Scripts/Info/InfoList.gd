extends Control


signal change_view_requested


var payload: Dictionary setget set_payload

# описание видимости блоков GUI
const GUI_SETUP: Dictionary = {
	"is_header_block_visible": true,
	"is_btn_back_block_visible": true,
	"is_score_block_visible": false,
	"is_level_block_visible": false,
	"is_footer_block_visible": false,
}


# BUILTINS -------------------------


# METHODS -------------------------


# SETGET -------------------------


func set_payload(dict: Dictionary) -> void:
	payload = dict


# SIGNALS -------------------------


func _on_BtnLvl1_pressed() -> void:
	emit_signal("change_view_requested", Global.get("VIEW_MAP").INFO, { "article": 1 })


func _on_BtnLvl2_pressed() -> void:
	emit_signal("change_view_requested", Global.get("VIEW_MAP").INFO, { "article": 2 })


func _on_BtnLvl3_pressed() -> void:
	emit_signal("change_view_requested", Global.get("VIEW_MAP").INFO, { "article": 3 })

