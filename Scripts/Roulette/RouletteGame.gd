extends Control


signal change_view_requested


# описание видимости блоков GUI
const GUI_SETUP: Dictionary = {
	"is_header_block_visible": true,
	"is_btn_back_block_visible": true,
	"is_score_block_visible": false,
	"is_level_block_visible": false,
	"is_footer_block_visible": false,
}

enum WHEEL_STATE {
	IDLE,
	INCREASING_SPEED,
	ROTATE,
	DECREASING_SPEED,
}


var wheel_state: int = WHEEL_STATE.IDLE

var SPIN_SPEED: float = 0.0
var SPEED: float = 2.5
const MAX_SPIN_SPEED: float = 400.0
var rot: float = 0.0

var is_timer_on: bool = false setget set_timer_on
const SECTION_COUNT: int = 8


# BUILTINS -------------------------


func _process(delta: float) -> void:
	if wheel_state != WHEEL_STATE.IDLE:
		# увеличение скорости
		if wheel_state == WHEEL_STATE.INCREASING_SPEED:
			SPIN_SPEED = SPIN_SPEED + SPEED if SPIN_SPEED + SPEED < MAX_SPIN_SPEED else MAX_SPIN_SPEED
			# кручение
			if SPIN_SPEED == MAX_SPIN_SPEED:
				wheel_state = WHEEL_STATE.ROTATE
		# уменьшение скорости
		elif wheel_state == WHEEL_STATE.DECREASING_SPEED:
			SPIN_SPEED = SPIN_SPEED - SPEED if SPIN_SPEED - SPEED > 0.0 else 0.0
			# остановка
			if SPIN_SPEED == 0.0:
				wheel_state = WHEEL_STATE.IDLE
				get_node("/root/Main/GUI").set("spinning", false)
				get_section()
		# при достижении максимальной скорости включение таймера кручения
		elif wheel_state == WHEEL_STATE.ROTATE and not is_timer_on:
			set_timer_on(true)
		rot += delta * SPIN_SPEED
		rot = wrapf(rot, 0.0, 360.0)
		($Wheel as TextureRect).rect_rotation = rot


# METHODS -------------------------


func get_section() -> void:
	var step: float = 360.0 / float(SECTION_COUNT)
	var section: int = SECTION_COUNT - round(rot / step) as int
	open_info(section)


func open_info(article: int) -> void:
	emit_signal("change_view_requested", Global.get("VIEW_MAP").INFO, { "article": article })


# SETGET -------------------------


func set_timer_on(state: bool) -> void:
	is_timer_on = state
	if state:
		($Timer as Timer).start()
	else:
		($Timer as Timer).stop()


# SIGNALS -------------------------


func _on_ButtonSpin_pressed() -> void:
	wheel_state = WHEEL_STATE.INCREASING_SPEED
	get_node("/root/Main/GUI").set("spinning", true)


func _on_Timer_timeout() -> void:
	set_timer_on(false)
	wheel_state = WHEEL_STATE.DECREASING_SPEED


